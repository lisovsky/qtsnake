#include <QApplication>
#include "snake.h"

int main(int argc, char *argv[]) {

  QApplication app(argc, argv);

  Snake window;

  window.setWindowTitle("Snake "+window.version);
  window.setFixedSize(Snake::getWidth(),Snake::getHeight());//(Snake::B_WIDTH,Snake::B_HEIGHT);
  window.show();

  return app.exec();
}
